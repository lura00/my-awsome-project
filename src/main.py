"""A very minimal API to showcase GitLab CI/CD pipelines.
"""

from fastapi import FastAPI
app = FastAPI()


@app.get("/")
def root():
    """The root endpoint, says hi to the user
    """
    return {"message": "Hello World"}
